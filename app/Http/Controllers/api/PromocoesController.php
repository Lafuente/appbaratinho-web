<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Promocao;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;

class PromocoesController extends Controller
{
    protected  $promocao = null;

    public function __construct(Promocao $promocao)
    {
        $this->promocao = $promocao;
    }

    public function index()
    {
        return response()->json([$this->promocao->getPromocoes()], 200);
    }

    public function create()
    {
        return View::make();
    }

    public function store()
    {
        return response()->json([$this->promocao->savePromocao()], 200);
    }

    public function edit($id)
    {
        $promocao = $this->promocao->getPromocao($id);
        if(!$promocao)
        {
            return response()->json(["response" => "Promoção não encontrada!"], 404);
        }
        return response()->json([$promocao], 200);
    }

    public function update($id)
    {
        $promocao = $this->promocao->updatePromocao($id);
        if(!$promocao)
        {
            return response()->json(["response" => "Promoção não encontrada!"], 404);
        }
        return response()->json([$promocao], 200);
    }

    public function delete($id)
    {
        $promocao = $this->promocao->deletePromocao($id);
        if(!$promocao)
        {
            return response()->json(["response" => "Promoção não encontrada!"], 404);
        }
        return response()->json(["response" => "Promoção apagada com sucesso!"], 200);
    }
}
